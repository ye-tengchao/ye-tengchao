# 叶腾超

####  **软件基础第三次作业** 
 **作业要求** 

实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。 扩展功能（加分项）：统计代码行、空行、注释行等，并提供相应命令接口。
 
**介绍及相关用法**
- 该项目用来统计一个纯英文的txt文件内的字符数，句子数，单词数，行数，空行数和注释数（注释前用//加以区分）。
- -c为字符数，-w为单词数，-s为句子数,-k为空行数，-t为换行数，-z为注释数。
 
**测试结果** 
- 计算字符数
![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/205138_b2ebf7b2_9706959.png "屏幕截图.png")
- 计算单词数
![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/205327_c69c5bc6_9706959.png "屏幕截图.png")
- 计算句子数
![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/205537_d7911803_9706959.png "屏幕截图.png")
- 计算行数
![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/210200_08c7b664_9706959.png "屏幕截图.png")
- 计算空行
![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/210407_b880bfd5_9706959.png "屏幕截图.png")
- 计算注释数
![输入图片说明](https://images.gitee.com/uploads/images/2021/1021/210519_9ff2426f_9706959.png "屏幕截图.png")